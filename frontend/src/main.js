import Vue from 'vue'
import App from './App.vue'
import * as VueGoogleMaps from "vue2-google-maps";
import VCalendar from 'v-calendar'

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyA1GYBT7K3s0j7u2v4HkSTNcpyX1ZuO7Wg",
        libraries: "places"
    }
});

Vue.use(VCalendar);


Vue.config.productionTip = false
new Vue({
    render: h => h(App),
}).$mount('#app')