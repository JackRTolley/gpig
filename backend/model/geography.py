"""
author -- PW, NV
filename -- geography.py

Defines classes for the physical geography of the model.
"""

import csv
import math
import json
from collections import namedtuple
from copy import deepcopy
import json
from os import listdir, path

from airquality import MAX_NO2, MAX_NOX, MAX_PARTICULATES

# Initial price for each zone
ZONE_INITIAL_PRICE = 1.0
# Decrease price if below this % of target
ON_TARGET_THRESHOLD_LOW = 0.95
# Increase price if above this % of target
ON_TARGET_THRESHOLD_HIGH = 1.00

MIN_CHARGE = 0
MAX_CHARGE = 5.0

NIGHT_START = 22
NIGHT_END = 6

"""
C-struct-like type for coordinates

Example initialisation:
coord = Coordinate(longitude=1, latitude=2)

Can be accessed like C-struct or python tuple:
coord.longitude
>> 1
coord[0]
>> 1
"""
Coordinate = namedtuple('Coordinate', ['longitude', 'latitude'])


class Zone:
    """City zone"""

    def __init__(self, display_name, outline_coordinates, hourly_targets):
        # Display
        self._display_name = display_name
        self._outline_coordinates = outline_coordinates

        # Status information
        self._vehicle_count = 0
        self._time = 0  # HACK PW Integer 0-23

        # Pricing
        self._hourly_targets = {}
        self._set_hourly_targets(hourly_targets)
        self._price = ZONE_INITIAL_PRICE

    def __repr__(self):
        return "Zone('%s', %s, %s)" % (
            self._display_name,
            self._outline_coordinates,
            self._hourly_targets
        )

    def _set_hourly_targets(self, hourly_targets):
        """
        Safely set hourly traffic targets
        Should be expressed as avg. instantaneous number of cars in zone over
        the course of an hour.

        Arguments:
        hourly_targets -- list of 24 targets (corresponding to hours of day)
        """
        if len(hourly_targets) != 24:
            raise ValueError(
                "There must be 24 hours worth of target congestion levels")

        self._hourly_targets.clear()
        self._hourly_targets = deepcopy(hourly_targets)

    def tick(self, avg_vehicles, air_quality=None, footfall=None):
        """
        Time tick function, to be called on the hour
        Updates pricing

        Arguments:
        avg_vehicles -- Average instantaneous number of vehicles on the road over the last hour
        air_quality -- AirQuality object detailing current air quality levels
        """
        self._vehicle_count = avg_vehicles
        self._time = (self._time + 1) % 24
        self._update_price(air_quality=air_quality, footfall=footfall)

    def _update_price(self, air_quality=None, footfall=None):
        """
        Update price based on information available
        TODO explain rationale

        Arguments:
        air_quality -- (optional) air quality data, None if not using
        """
        # 1) TARGET VEHICLE COUNT
        # get target vehicle count of next time slot
        target_vehicle_count = self._hourly_targets[self._time]

        # 2) EMISSIONS ADJUSTMENT
        # adjust this down if we are over any emissions limits
        if air_quality is not None:
            # find if any pollutants are above allowed levels
            # if any are, reduce the target_vehicle_count in proportion to the level of transgression
            max_transgression = max(air_quality.NO2/MAX_NO2, air_quality.NOX/MAX_NOX, air_quality.particulates/MAX_PARTICULATES)
            if max_transgression > 1:
                # TODO PW - work out how much to adjust
                target_vehicle_count = target_vehicle_count / max_transgression

        # 2.2) FOOTFALL ADJUSTMENT
        if footfall is not None:
            target_vehicle_count = max(100, target_vehicle_count * ((3000 - footfall)/3000))

        # 3) REACT

        # NOTE PW - commented this out to trial a different pricing mechanism as this doesn't move much
        # if self._vehicle_count > (ON_TARGET_THRESHOLD_HIGH * target_vehicle_count):
        #     self._price += 0.1
        # elif self._vehicle_count < (ON_TARGET_THRESHOLD_LOW * target_vehicle_count):
        #     self._price -= 0.1
        # if self._price < MIN_CHARGE: self._price = MIN_CHARGE
        # elif self._price > MAX_CHARGE: self._price = MAX_CHARGE

        # new_price = math.sqrt(self._vehicle_count / target_vehicle_count) * ZONE_INITIAL_PRICE
        new_price = pow(self._vehicle_count / target_vehicle_count, 0.75) * ZONE_INITIAL_PRICE
        new_price = max(MIN_CHARGE, new_price)
        new_price = min(MAX_CHARGE, new_price)
        self._price = new_price

    def get_display_name(self):
        return self._display_name

    def get_outline_coordinates(self):
        return self._outline_coordinates

    def get_hourly_targets(self):
        return self._hourly_targets

    def get_price(self):
        return self._price

    def get_vehicle_count(self):
        return self._vehicle_count

    @classmethod
    def from_geojson(cls, filepath, use_demographics=True):
        # read raw data from file
        with open(filepath, 'r') as file:
            rawdata = file.read()
        
        # parse json
        data = json.loads(rawdata)
        json_coords = data['coordinates'][0]

        # create coordinate objects for each coordinate
        coords = []
        for json_coord in json_coords:
            coord = Coordinate(longitude=json_coord[0], latitude=json_coord[1])
            coords.append(coord)

        # create zone details
        zone_id = path.split(filepath)[1].split('.')[0]
        display_name = ' '.join([word[0].upper() + word[1:] for word in zone_id.split('_')])

        if use_demographics:
            # if we are using demographic data, fetch it from the file
            # demographics.csv file must be placed in the same folder as the .geojson file
            # each row must be of form zone_id, population, area
            with open(path.join(path.dirname(filepath), 'demographics.csv'), 'r') as file:
                dict_reader = csv.DictReader(file)

                for row in dict_reader:
                    # get ward population and area data
                    if row['Ward'] == zone_id:
                        population = float(row['Population'])
                        area = float(row['Area']) / 1000000  # convert to km^2
                        break

                try:
                    density = population / area
                except NameError:
                    raise ValueError('Ward "%s" is not in "%s/demographics.csv"' % (zone_id, filepath))

                # NOTE PW
                #
                # Intitial attempt at setting hourly targets
                # 
                # Based on manual count data from York council, you generally see avg. of c. 500
                # vehicles through a checkpoint every hour (obviously varies through day),
                # I've guessed 500 vehicles in whole zone at any given point is a reasonable
                # maximum.
                #
                # This initial attempt sets all hourly targets to 500 and adjusts based
                # on the population density of a zone during high traffic.
                traffic_targets = [500, ] * 24
                for i in range(24):
                    if i < NIGHT_END or i >= NIGHT_START:
                        traffic_targets[i] = traffic_targets[i] / (density / 2500)

        else:
            # if we are not using demographic data, set the hourly targets
            # to an arbitrary number
            traffic_targets = [1000, ] * 24

        return zone_id, cls(display_name, coords, traffic_targets)


class City:
    def __init__(self, zones=None):
        self._zones = {}
        self.add_zones(zones)

    def __repr__(self):
        return "City(zones=%s)" % self._zones

    def add_zone(self, zone_id, zone):
        """
        Add a single zone to the city
        Arguments:
        zone_id -- unique identifier for the zone
        zone -- zone object
        """
        if zone_id in self._zones:
            raise ValueError("Zone identifier '%s' is not unique" % zone_id)
        if not isinstance(zone, Zone):
            raise ValueError("Supplied zone argument is not a valid Zone object. Actual type is: %s" % type(zone))
        
        self._zones[zone_id] = zone

    def add_zones(self, new_zones):
        """
        More efficient way to add multiple zones to the city

        Arguments:
        zones -- dictionary, where keys are zone IDs and values are corresponding
                 zone objects
        """
        # Ensure all elements are Zone objects
        for zone in new_zones.values():
            if not isinstance(zone, Zone):
                raise ValueError("Supplied zone argument is not a valid Zone object. Actual type is: %s" % type(zone))
        
        # merge dicts of existing and new zones
        self._zones.update(new_zones)

    def get_zones(self):
        return self._zones

    @classmethod
    def from_folder(self, folderpath):
        filepaths = [path.join(folderpath, f) for f in listdir(folderpath) if f[-8:] == '.geojson']

        # create zones
        zones = {}
        for filepath in filepaths:
            zone_id, zone = Zone.from_geojson(filepath, folderpath)
            if zone_id in zones:
                raise ValueError("Zone identifier '%s' is not unique" % zone_id)
            zones[zone_id] = zone

        return City(zones)


