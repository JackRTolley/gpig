import csv
from datetime import datetime

DATE_FORMAT = "%d/%m/%Y %H:%M:%S"

STREETS = {
    'micklegate': ['Micklegate'],
    'guildhall': ['Stonegate', 'Parliament Street', 'Coney Street']
}


class FootfallHandler:
    def __init__(self, filepath="york/footfall.csv"):
        self._data = {}

        with open(filepath, 'r', newline="") as file:
            dictreader = csv.DictReader(file)

            for row in dictreader:
                dt = datetime.strptime(row['Date'], DATE_FORMAT)
                street = row['LocationName']
                try:
                    count = int(row['InCount'])
                except ValueError:
                    count = 0

                if dt not in self._data:
                    self._data[dt] = {}

                self._data[dt][street] = count

    def get_zone_footfall(self, dt, zone_id):
        dt = dt.replace(year=2019, minute=0, second=0, microsecond=0)

        try:
            streets = STREETS[zone_id]
        except KeyError:
            return 0

        retval = 0
        for street in streets:
            retval = retval + self._data[dt][street]

        if retval == 0:
            # HACK PW - if we don't have data for this zone, set based on
            # hour of day
            if dt.hour < 8:
                retval = 10
            elif dt.hour < 12:
                retval = 50
            elif dt.hour < 18:
                retval = 75
            elif dt.hour < 22:
                retavl = 25
            else:
                retval = 10

        return retval


if __name__ == '__main__':
    fh = FootfallHandler()
    dt = datetime(2019, 4, 4, 12)
    print(fh.get_zone_footfall(dt, 'micklegate'))
    print(fh.get_zone_footfall(dt, 'guildhall'))
    print(fh.get_zone_footfall(dt, 'otherzone'))
