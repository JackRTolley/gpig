import sqlite3 as sql
from datetime import datetime, timedelta
from http import HTTPStatus
from os import path

from flask import Flask, g, request
from flask_cors import CORS
from flask_restful import Api, Resource
from flask_restful.reqparse import RequestParser


class Zones(Resource):
    """
    Collection of zones endpoint.
    """

    @classmethod
    def get_endpoint_path(cls):
        """
        Returns the path to this endpoint.
        """
        return '/zones'

    def get(self):
        """
        The GET endpoint.
        Fetches all of the zones from the database and returns some basic high-level information for each one.
        :return:
            Either
                A JSON object with a 'zones' field containing a list of all the zones, each in a JSON format, as defined
                by the 'zone_db_to_json' function.
                Response code 200.
            Or
                An empty JSON object if no zones exist in the database.
                Response code 204.
        """
        zone_ids = g.db_cursor.execute('SELECT ID FROM Zones').fetchall()
        if len(zone_ids) == 0:
            return {}, HTTPStatus.NO_CONTENT

        return {'zones': [zone_db_to_json(zone_id[0], name=True) for zone_id in zone_ids]}, HTTPStatus.OK


class Zone(Resource):
    """
    Individual zone endpoint.
    """

    @classmethod
    def get_endpoint_path(cls):
        """
        Returns the path to this endpoint.
        """
        return '/zones/<zone_id>'

    def get(self, zone_id):
        """
        The GET endpoint.
        Fetches the specified zone from the database and returns all of the data related to it.
        :param zone_id: Specifies the zone to fetch.
        :return:
            Either
                The zone in a JSON format, as defined by the 'zone_db_to_json' function.
                Response code 200.
            Or
                A JSON object with an 'error' field if a zone with the specified ID doesn't exist.
                Response code 404.
        """
        zone = zone_db_to_json(zone_id, name=True, coordinates=True, latest_price=True)
        if not zone:
            return {'error': 'No zone with id {}'.format(zone_id)}, HTTPStatus.NOT_FOUND

        return zone, HTTPStatus.OK


class Price(Resource):
    """
    Price for an individual zone endpoint.
    """

    @classmethod
    def get_endpoint_path(cls):
        """
        Returns the path to this endpoint.
        """
        return '/zones/<zone_id>/price'

    def get(self, zone_id):
        """
        The GET endpoint.
        Retrieves the price set in a specified zone at a specified date and hour. If a price wasn't set during that hour
        then the most recent price up to the specified datetime is returned.
        Query parameters (all integers):
            year
            month
            day
            hour
        If a parameter isn't supplied then the corresponding value of the current date and time is used.
        :param zone_id: Specifies the zone to fetch.
        :return:
            Either
                A JSON object with a 'price' field containing the price of the zone and a 'datetime' field containing
                the date and time that price was set.
                Response code 200.
            Or
                A JSON object with an 'error' field if an invalid date or time is provided.
                Response code 400.
            Or
                A JSON object with an 'error' field if a zone with the specified ID doesn't exist.
                Response code 404.
            Or
                An empty JSON object is no price data exists for the specified zone before the specified date and time.
                Response code 204.
        """
        parser = RequestParser()
        current_time = datetime.now()
        parser.add_argument('year', default=current_time.year)
        parser.add_argument('month', default=current_time.month)
        parser.add_argument('day', default=current_time.day)
        parser.add_argument('hour', default=current_time.hour)
        args = parser.parse_args()

        try:
            parsed_time = datetime(year=int(args['year']), month=int(args['month']), day=int(args['day']),
                                   hour=int(args['hour']))
        except (ValueError, TypeError):
            return {'error': 'Invalid datetime'}, HTTPStatus.BAD_REQUEST

        zone = zone_db_to_json(zone_id)
        if not zone:
            return {'error': 'No zone with id {}'.format(zone_id)}, HTTPStatus.NOT_FOUND

        price_row = g.db_cursor.execute(
            'SELECT DateTime, Price FROM Prices WHERE ZoneID = ? AND DateTime <= ? ORDER BY DateTime DESC LIMIT 1',
            (zone_id, parsed_time.strftime('%Y-%m-%d %H:%M:%S'))).fetchone()

        if not price_row:
            return {}, HTTPStatus.NO_CONTENT

        json_price = {
            'price': price_row[1],
            'datetime': price_row[0]
        }

        return json_price, HTTPStatus.OK


class DailyPrices(Resource):
    """
    Collection of prices in a single day for an individual zone endpoint.
    """

    @classmethod
    def get_endpoint_path(cls):
        """
        Returns the path to this endpoint.
        """
        return '/zones/<zone_id>/price/daily'

    def get(self, zone_id):
        """
        The GET endpoint.
        Retrieves the list of prices set in a specified zone at a specified date. All prices set for the specified date
        will be returned. This will usually mean 24 entries, one for each hour, but could be less.
        Query parameters (all integers):
            year
            month
            day
        If a parameter isn't supplied then the corresponding value of the current date and time is used.
        :param zone_id: Specifies the zone to fetch.
        :return:
            Either
                A JSON object with a 'prices' field containing the prices of the zone. Each price will have a 'price'
                field and a 'datetime' that corresponds to the time this price is set for.
                Response code 200.
            Or
                A JSON object with an 'error' field if an invalid date or time is provided.
                Response code 400.
            Or
                A JSON object with an 'error' field if a zone with the specified ID doesn't exist.
                Response code 404.
            Or
                An empty JSON object is no price data exists for the specified zone on the specified date.
                Response code 204.
        """
        parser = RequestParser()
        current_time = datetime.now()
        parser.add_argument('year', default=current_time.year)
        parser.add_argument('month', default=current_time.month)
        parser.add_argument('day', default=current_time.day)
        args = parser.parse_args()

        try:
            parsed_time = datetime(year=int(args['year']), month=int(args['month']), day=int(args['day']))
        except (ValueError, TypeError):
            return {'error': 'Invalid datetime'}, HTTPStatus.BAD_REQUEST

        zone = zone_db_to_json(zone_id)
        if not zone:
            return {'error': 'No zone with id {}'.format(zone_id)}, HTTPStatus.NOT_FOUND

        price_rows = g.db_cursor.execute(
            'SELECT DateTime, Price FROM Prices WHERE ZoneID = ? AND DateTime >= ? AND DateTime < ? ' +
            'ORDER BY DateTime ASC',
            (zone_id, parsed_time.strftime('%Y-%m-%d %H:%M:%S'),
             (parsed_time + timedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S'))).fetchall()

        if not price_rows:
            return {}, HTTPStatus.NO_CONTENT

        json_prices = {
            'prices': [
                {
                    'price': price_row[1],
                    'datetime': price_row[0]
                }
                for price_row in price_rows
            ]
        }

        return json_prices, HTTPStatus.OK


class Events(Resource):
    """
    Collection of events endpoint.
    """

    @classmethod
    def get_endpoint_path(cls):
        """
        Returns the path to this endpoint.
        """
        return '/events'

    def get(self):
        """
        The GET endpoint.
        Fetches all of the events from the database and returns all the data related to each one.
        :return:
            Either
                A JSON object with a 'events' field containing a list of all the events, each in a JSON format, as
                defined by the 'event_db_to_json' function.
                Response code 200.
            Or
                An empty JSON object if no events exist in the database.
                Response code 204.
        """
        event_ids = g.db_cursor.execute('SELECT EventID FROM Events').fetchall()
        if len(event_ids) == 0:
            return {}, HTTPStatus.NO_CONTENT

        return {'events': [
            event_db_to_json(event_id[0], zone_id=True, event_type=True, name=True, description=True,
                             start_datetime=True, end_datetime=True)
            for event_id in event_ids]}, HTTPStatus.OK

    def post(self):
        """
        The POST endpoint.
        Inserts a new event into the database. Data for the new event is retrieved from the request body in a JSON
        format. At the least this JSON object must include the fields 'zoneId', 'type', 'name', 'startDatetime' and
        'endDatetime', but can include an optional 'description' field.
        :return:
            Either
                The new event in a JSON format, as defined by the 'event_db_to_json' function.
                Response code 201.
            Or
                A JSON object with an 'error' field if required body fields were missing.
                Response code 400.
            Or
                A JSON object with an 'error' field if the start and/or end datetime was provided in an invalid format.
                Response code 400.
            Or
                A JSON object with an 'error' field if the start datetime was later than the end datetime.
                Response code 400.
            Or
                A JSON object with an 'error' field if an invalid zoneId was provided.
                Response code 400.
            Or
                A JSON object with an 'error' field if an error occurred inserting the event into the database.
                Response code 500.
        """
        body = request.get_json()
        if 'zoneId' not in body or 'type' not in body or 'name' not in body or 'startDatetime' not in body or \
                'endDatetime' not in body:
            return {'error': 'The body of the request must contain the JSON fields ' +
                             '"zoneId", "type", "name", "startDatetime" and "endDatetime"'}, HTTPStatus.BAD_REQUEST

        try:
            parsed_start_time = datetime.strptime(body['startDatetime'], '%Y-%m-%d %H:%M:%S')
            parsed_end_time = datetime.strptime(body['endDatetime'], '%Y-%m-%d %H:%M:%S')
        except (ValueError, TypeError):
            return {'error': 'Invalid startDatetime and/or endDatetime'}, HTTPStatus.BAD_REQUEST

        if parsed_end_time <= parsed_start_time:
            return {'error': 'The start datetime must be earlier than the end datetime'}, HTTPStatus.BAD_REQUEST

        if not zone_db_to_json(body['zoneId']):
            return {'error': 'Invalid zoneId'}, HTTPStatus.BAD_REQUEST

        if 'description' not in body:
            body['description'] = ''

        g.db_cursor.execute(
            'INSERT INTO Events (ZoneID, Type, Name, Description, StartDateTime, EndDateTime) VALUES (?, ?, ?, ?, ?, ?)',
            (body['zoneId'], body['type'], body['name'], body['description'], body['startDatetime'],
             body['endDatetime']))

        inserted_event = event_db_to_json(g.db_cursor.lastrowid, zone_id=True, event_type=True, name=True,
                                          description=True, start_datetime=True, end_datetime=True)
        if not inserted_event:
            return {'error': 'An unknown error occurred when inserting into the database'}, \
                   HTTPStatus.INTERNAL_SERVER_ERROR

        return inserted_event, HTTPStatus.CREATED


class Event(Resource):
    """
    Individual event endpoint.
    """

    @classmethod
    def get_endpoint_path(cls):
        """
        Returns the path to this endpoint.
        """
        return '/events/<event_id>'

    def get(self, event_id):
        """
        The GET endpoint.
        Fetches the specified event from the database and returns all of the data related to it.
        :param event_id: Specifies the event to fetch.
        :return:
            Either
                The event in a JSON format, as defined by the 'event_db_to_json' function.
                Response code 200.
            Or
                A JSON object with an 'error' field if a event with the specified ID doesn't exist.
                Response code 404.
        """
        event = event_db_to_json(event_id, zone_id=True, event_type=True, name=True, description=True,
                                 start_datetime=True, end_datetime=True)
        if not event:
            return {'error': 'No event with id {}'.format(event_id)}, HTTPStatus.NOT_FOUND

        return event, HTTPStatus.OK

    def delete(self, event_id):
        """
        The DELETE endpoint.
        Deletes the specified event from the database.
        :param event_id: Specifies the event to delete.
        :return:
            Either
                An empty JSON object if the delete operation was successful.
                Response code 204.
            Or
                A JSON object with an 'error' field if a event with the specified ID doesn't exist.
                Response code 404.
        """
        event = event_db_to_json(event_id)
        if not event:
            return {'error': 'No event with id {}'.format(event_id)}, HTTPStatus.NOT_FOUND

        g.db_cursor.execute('DELETE FROM Events WHERE EventID = ?', (event_id,))
        return {}, HTTPStatus.NO_CONTENT


class Passes(Resource):
    """
    Collection of passes endpoint.
    """

    @classmethod
    def get_endpoint_path(cls):
        """
        Returns the path to this endpoint.
        """
        return '/passes'

    def get(self):
        """
        The GET endpoint.
        Fetches all of the passes from the database and returns all the data related to each one.
        :return:
            Either
                A JSON object with a 'passes' field containing a list of all the passes, each in a JSON format, as
                defined by the 'pass_db_to_json' function.
                Response code 200.
            Or
                An empty JSON object if no passes exist in the database.
                Response code 204.
        """
        pass_ids = g.db_cursor.execute('SELECT PassID FROM Passes').fetchall()
        if len(pass_ids) == 0:
            return {}, HTTPStatus.NO_CONTENT

        return {'passes': [
            pass_db_to_json(pass_id[0], pass_type=True, price=True, start_datetime=True, end_datetime=True)
            for pass_id in pass_ids]}, HTTPStatus.OK

    def post(self):
        """
        The POST endpoint.
        Inserts a new pass into the database. Data for the new pass is retrieved from the request body in a JSON
        format. This JSON object must include the fields 'type', 'price', 'startDatetime' and 'endDatetime'.
        :return:
            Either
                The new pass in a JSON format, as defined by the 'pass_db_to_json' function.
                Response code 201.
            Or
                A JSON object with an 'error' field if required body fields were missing.
                Response code 400.
            Or
                A JSON object with an 'error' field if the start and/or end datetime was provided in an invalid format.
                Response code 400.
            Or
                A JSON object with an 'error' field if the start datetime was later than the end datetime.
                Response code 400.
            Or
                A JSON object with an 'error' field if an error occurred inserting the event into the database.
                Response code 500.
        """
        body = request.get_json()
        if 'type' not in body or 'price' not in body or 'startDatetime' not in body or 'endDatetime' not in body:
            return {'error': 'The body of the request must contain the JSON fields ' +
                             '"type", "price", "startDatetime" and "endDatetime"'}, HTTPStatus.BAD_REQUEST

        try:
            parsed_start_time = datetime.strptime(body['startDatetime'], '%Y-%m-%d %H:%M:%S')
            parsed_end_time = datetime.strptime(body['endDatetime'], '%Y-%m-%d %H:%M:%S')
        except (ValueError, TypeError):
            return {'error': 'Invalid startDatetime and/or endDatetime'}, HTTPStatus.BAD_REQUEST

        if parsed_end_time <= parsed_start_time:
            return {'error': 'The start datetime must be earlier than the end datetime'}, HTTPStatus.BAD_REQUEST

        g.db_cursor.execute('INSERT INTO Passes (Type, Price, StartDateTime, EndDateTime) VALUES (?, ?, ?, ?)',
                            (body['type'], body['price'], body['startDatetime'], body['endDatetime']))

        inserted_pass = pass_db_to_json(g.db_cursor.lastrowid, pass_type=True, price=True, start_datetime=True,
                                        end_datetime=True)
        if not inserted_pass:
            return {'error': 'An unknown error occurred when inserting into the database'}, \
                   HTTPStatus.INTERNAL_SERVER_ERROR

        return inserted_pass, HTTPStatus.CREATED


class Pass(Resource):
    """
    Individual pass endpoint.
    """

    @classmethod
    def get_endpoint_path(cls):
        """
        Returns the path to this endpoint.
        """
        return '/passes/<pass_id>'

    def get(self, pass_id):
        """
        The GET endpoint.
        Fetches the specified pass from the database and returns all of the data related to it.
        :param pass_id: Specifies the pass to fetch.
        :return:
            Either
                The event in a JSON format, as defined by the 'pass_db_to_json' function.
                Response code 200.
            Or
                A JSON object with an 'error' field if a pass with the specified ID doesn't exist.
                Response code 404.
        """
        pass_ = pass_db_to_json(pass_id, pass_type=True, price=True, start_datetime=True, end_datetime=True)
        if not pass_:
            return {'error': 'No pass with id {}'.format(pass_id)}, HTTPStatus.NOT_FOUND

        return pass_, HTTPStatus.OK

    def delete(self, pass_id):
        """
        The DELETE endpoint.
        Deletes the specified pass from the database.
        :param pass_id: Specifies the pass to delete.
        :return:
            Either
                An empty JSON object if the delete operation was successful.
                Response code 204.
            Or
                A JSON object with an 'error' field if a pass with the specified ID doesn't exist.
                Response code 404.
        """
        pass_ = pass_db_to_json(pass_id)
        if not pass_:
            return {'error': 'No pass with id {}'.format(pass_id)}, HTTPStatus.NOT_FOUND

        g.db_cursor.execute('DELETE FROM Passes WHERE PassID = ?', (pass_id,))
        return {}, HTTPStatus.NO_CONTENT


def zone_db_to_json(zone_id, name=False, coordinates=False, latest_price=False):
    """
    Translates a database Zone object into its JSON (dict) form.
    :param zone_id: The id of the zone to convert to JSON.
    :param name: If True then a 'name' field is included in the JSON.
    :param coordinates: If True then a 'coordinates' field is included in the JSON.
    :param latest_price: If True then a 'latestPrice' field is included in the JSON.
    :return: A JSON (dict) representation of the specified zone, or None if the zone doesn't exist.
        Example:
        {
            'id': 'zone1',
            'name': 'Hull Road',
            'coordinates': [
                {
                    'longitude': '1.234'
                    'latitude': '2.345'
                },
                {
                    'longitude': '3.456'
                    'latitude': '4.567'
                },
                {
                    'longitude': '5.678'
                    'latitude': '6.789'
                }
            ],
            'latestPrice': {
                'price': 1.40,
                'lastUpdate': '2020-05-04 16:00:00'.
            }
        }
    """
    json_zone = dict()

    zone_row = g.db_cursor.execute('SELECT ID, Name FROM Zones WHERE ID = ?', (zone_id,)).fetchone()
    if not zone_row:
        return None

    json_zone['id'] = zone_row[0]

    if name:
        json_zone['name'] = zone_row[1]

    if coordinates:
        coord_rows = g.db_cursor.execute('SELECT Longitude, Latitude FROM Coordinates WHERE ZoneID = ?',
                                         (zone_id,)).fetchall()
        json_zone['coordinates'] = [
            {
                'longitude': coord[0],
                'latitude': coord[1]
            }
            for coord in coord_rows
        ]

    if latest_price:
        latest_price_row = g.db_cursor.execute(
            'SELECT DateTime, Price FROM Prices WHERE ZoneID = ? ORDER BY DateTime DESC LIMIT 1', (zone_id,)).fetchone()

        if latest_price_row:
            json_zone['latestPrice'] = {
                'price': latest_price_row[1],
                'datetime': latest_price_row[0]
            }

    return json_zone


def event_db_to_json(event_id, zone_id=False, event_type=False, name=False, description=False, start_datetime=False,
                     end_datetime=False):
    """
    Translates a database Event object into its JSON (dict) form.
    :param event_id: The id of the event to convert to JSON.
    :param zone_id: If True then a 'zoneId' field is included in the JSON.
    :param event_type: If True then a 'type' field is included in the JSON.
    :param name: If True then a 'name' field is included in the JSON.
    :param description: If True then a 'description' field is included in the JSON.
    :param start_datetime: If True then a 'startDatetime' field is included in the JSON.
    :param end_datetime: If True then a 'endDatetime' field is included in the JSON.
    :return: A JSON (dict) representation of the specified event, or None if the event doesn't exist.
        Example:
        {
            'id': 21,
            'zoneId': 'hull_road',
            'type': 'Planned',
            'name': 'Lemonade Parade',
            'description': 'When life gives you lemons... Parade',
            'startDatetime': '2020-05-04 16:00:00',
            'endDatetime': '2020-05-05 06:30:00'
        }
    """
    json_event = dict()

    event_row = g.db_cursor.execute(
        'SELECT EventID, ZoneID, Type, Name, Description, StartDateTime, EndDateTime FROM Events WHERE EventID = ?',
        (event_id,)).fetchone()
    if not event_row:
        return None

    json_event['id'] = event_row[0]

    if zone_id:
        json_event['zoneId'] = event_row[1]

    if event_type:
        json_event['type'] = event_row[2]

    if name:
        json_event['name'] = event_row[3]

    if description:
        json_event['description'] = event_row[4]

    if start_datetime:
        json_event['startDatetime'] = event_row[5]

    if end_datetime:
        json_event['endDatetime'] = event_row[6]

    return json_event


def pass_db_to_json(pass_id, pass_type=False, price=False, start_datetime=False, end_datetime=False):
    """
    Translates a database Pass object into its JSON (dict) form.
    :param pass_id: The id of the pass to convert to JSON.
    :param pass_type: If True then a 'type' field is included in the JSON.
    :param price: If True then a 'price' field is included in the JSON.
    :param start_datetime: If True then a 'startDatetime' field is included in the JSON.
    :param end_datetime: If True then a 'endDatetime' field is included in the JSON.
    :return: A JSON (dict) representation of the specified pass, or None if the event doesn't exist.
        Example:
        {
            'id': 8,
            'type': 'City',
            'price': 3.50,
            'startDatetime': '2020-05-04 16:00:00',
            'endDatetime': '2020-05-05 06:30:00'
        }
    """
    json_pass = dict()

    pass_row = g.db_cursor.execute(
        'SELECT PassID, Type, Price, StartDateTime, EndDateTime FROM Passes WHERE PassID = ?', (pass_id,)).fetchone()
    if not pass_row:
        return None

    json_pass['id'] = pass_row[0]

    if type:
        json_pass['type'] = pass_row[1]

    if price:
        json_pass['price'] = pass_row[2]

    if start_datetime:
        json_pass['startDatetime'] = pass_row[3]

    if end_datetime:
        json_pass['endDatetime'] = pass_row[4]

    return json_pass


def connect_to_database():
    """
    Creates objects to access the database and stores them in the global environment.
    Initialised global variables:
        g.db_connection - sqlite3 Connection object
        g.db_cursor - sqlite3 Cursor object
    """
    database_path = 'congestioncharge.db'
    if path.exists(database_path):
        g.db_connection = sql.connect(database_path)
        g.db_cursor = g.db_connection.cursor()
    else:
        return {'error': 'The system database does not exist'}, HTTPStatus.INTERNAL_SERVER_ERROR


def close_database_connection(response):
    """
    Close the connection made to the database by the global environment.
    Any changes made to the database are committed and then the connection is closed.
    """
    if response.status.startswith('500'):
        return response
    g.db_connection.commit()
    g.db_connection.close()
    return response


def start_api():
    # Flask initialisation.
    app = Flask(__name__)
    CORS(app)
    api = Api(app)

    # Establish endpoints.
    api.add_resource(Zones, Zones.get_endpoint_path())
    api.add_resource(Zone, Zone.get_endpoint_path())
    api.add_resource(Price, Price.get_endpoint_path())
    api.add_resource(DailyPrices, DailyPrices.get_endpoint_path())
    api.add_resource(Events, Events.get_endpoint_path())
    api.add_resource(Event, Event.get_endpoint_path())
    api.add_resource(Passes, Passes.get_endpoint_path())
    api.add_resource(Pass, Pass.get_endpoint_path())

    # Connect to the database before servicing each request.
    app.before_request(connect_to_database)
    app.after_request(close_database_connection)

    # Run web server.
    app.run(host='0.0.0.0', port=80, debug=True)


if __name__ == '__main__':
    start_api()
